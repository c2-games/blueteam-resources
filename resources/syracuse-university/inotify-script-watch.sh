#!/bin/bash

# Usage:
# ./inotify-script.watch.sh "watch-dir-path" "watch-log-path" >/dev/null 2>&1  &
#
# inotify-script-watch.sh

watchdir=$1
logfile=$2
while : ; do
        inotifywait $watchdir|while read path action file; do
                ts=$(date +"%C%y%m%d%H%M%S")
                echo "$ts :: file: $file :: $action :: $path">>$logfile
        done
done
exit 0
